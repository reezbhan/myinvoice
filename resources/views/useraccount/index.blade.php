<html>
<head>
    <title>All user</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>



	<div class="container">
	<div class="row">
		<div class="span5">
            <table class="table table-striped table-condensed">
                  <thead>
                  <tr>
                      <th>accountName</th>
                      <th>date</th>
                      <th>description</th>
                      <th>category</th>
                      <th>income</th> 
                      <th>expence</th> 
                      <th>debit_or_credit</th> 
                      <th>total</th>                                           
                  </tr>
              </thead>   
              <tbody>

              	@foreach($alldata as $data)
	                <tr>
	                    <td>{{$data->accountName}}</td>
	                    <td>{{$data->date}}</td>
	                    <td>{{$data->description}}</td>
	                    <td>{{$data->category}}</td>
	                    <td>{{$data->income}}</td>
	                    <td>{{$data->expence}}</td>
	                    <td>{{$data->debit_or_credit}}</td>
	                    <td>{{$data->total}}</td>
	                    
	                    <td><span class="label label-success">
	                    PDF</span>
	                    </td>                                       
	                </tr>
	            @endforeach
                                                  
              </tbody>
            </table>
            </div>
	</div>
</div>


    
</body>