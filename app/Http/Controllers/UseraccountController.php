<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserAccount;

class UseraccountController extends Controller
{
    //


	public function index()
	{

		$alldata=UserAccount::all();
		return view('useraccount.index',compact('alldata'));


	}

	public function create()
	{

		return view('useraccount.create');
	}

	public function store(Request $request)
	{

		$input = $request->all();
		UserAccount::create($input);
		return redirect('useraccount');


	}

}
