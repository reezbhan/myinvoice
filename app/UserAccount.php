<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    protected $table='useraccount';
    protected $primaryKey="user_id";

    protected $fillable=['accountName',
						'date',
						'description',
						'category',
						'income',
						'expence',
						'debit_or_credit',
						'total'
					];
}
